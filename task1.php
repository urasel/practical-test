<?php 
	include 'layout/header.php';
	include 'config/db.php';

	
	echo '<div class="container">';
		$sql = "
		SELECT cat.Name, COUNT(itm.categoryId) AS item FROM category cat 
		JOIN item_category_relations itm ON cat.Id = itm.categoryId 
		GROUP BY cat.Id 
		ORDER BY item DESC
		";
		$result = $conn->query($sql);
	
		if ($result->num_rows > 0) {
		?>
			<table class="table table-hover">
				<thead>
				<tr>
					<th>Category Name</th>
					<th>Total Items</th>
				</tr>
				</thead>
				<tbody>
				<?php while($row = $result->fetch_assoc()) { ?>
					<tr>
						<td><?= $row["Name"] ?></td>
						<td><?= $row["item"] ?></td>
					</tr>
				<?php } ?>
				</tbody>
				</table>
				<?php
		
		}
	echo '</div>';
  
	$conn->close();
	include 'layout/footer.php'; 
?>
